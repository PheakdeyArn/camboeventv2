Part 1: Deployment
2. Remote to server
$ ssh root@174.138.23.82
password: !@#Somngat0000c
3. navigate to project directory
cd /home/camboeventv2/
4. First Build for the web service by command
docker-compose -f docker-compose.prod.yml up --build -d
5. 
6. Optional: we can stop service
docker-compose -f docker-compose.prod.yml stop
7. To start service after it's already built
docker-compose -f docker-compose.prod.yml start
8. Re-deploy

Extra Note:

8. Optional: we can stop service and delete all data
9. docker-compose -f docker-compose.prod.yml down -v